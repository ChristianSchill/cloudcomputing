﻿using Generated;
using Grpc.Core;
using System.Threading.Tasks;

namespace Server
{
    internal class MessageOperationService : Generated.MessageResponse.MessageResponseBase
    {
        public override Task<OperationResponse> Greeting(Generated.MessageRequest request, ServerCallContext context)
        {
            System.Console.WriteLine("Hello there General " + request.Name);

            var result = "";

            return Task.FromResult(new OperationResponse() { Message = result });
        }
    }
}
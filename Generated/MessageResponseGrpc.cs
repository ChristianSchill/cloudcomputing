// <auto-generated>
//     Generated by the protocol buffer compiler.  DO NOT EDIT!
//     source: MessageResponse.proto
// </auto-generated>
#pragma warning disable 0414, 1591
#region Designer generated code

using grpc = global::Grpc.Core;

namespace Generated {
  public static partial class MessageResponse
  {
    static readonly string __ServiceName = "MessageResponse";

    static readonly grpc::Marshaller<global::Generated.MessageRequest> __Marshaller_MessageRequest = grpc::Marshallers.Create((arg) => global::Google.Protobuf.MessageExtensions.ToByteArray(arg), global::Generated.MessageRequest.Parser.ParseFrom);
    static readonly grpc::Marshaller<global::Generated.OperationResponse> __Marshaller_OperationResponse = grpc::Marshallers.Create((arg) => global::Google.Protobuf.MessageExtensions.ToByteArray(arg), global::Generated.OperationResponse.Parser.ParseFrom);

    static readonly grpc::Method<global::Generated.MessageRequest, global::Generated.OperationResponse> __Method_Greeting = new grpc::Method<global::Generated.MessageRequest, global::Generated.OperationResponse>(
        grpc::MethodType.Unary,
        __ServiceName,
        "Greeting",
        __Marshaller_MessageRequest,
        __Marshaller_OperationResponse);

    /// <summary>Service descriptor</summary>
    public static global::Google.Protobuf.Reflection.ServiceDescriptor Descriptor
    {
      get { return global::Generated.MessageResponseReflection.Descriptor.Services[0]; }
    }

    /// <summary>Base class for server-side implementations of MessageResponse</summary>
    [grpc::BindServiceMethod(typeof(MessageResponse), "BindService")]
    public abstract partial class MessageResponseBase
    {
      public virtual global::System.Threading.Tasks.Task<global::Generated.OperationResponse> Greeting(global::Generated.MessageRequest request, grpc::ServerCallContext context)
      {
        throw new grpc::RpcException(new grpc::Status(grpc::StatusCode.Unimplemented, ""));
      }

    }

    /// <summary>Client for MessageResponse</summary>
    public partial class MessageResponseClient : grpc::ClientBase<MessageResponseClient>
    {
      /// <summary>Creates a new client for MessageResponse</summary>
      /// <param name="channel">The channel to use to make remote calls.</param>
      public MessageResponseClient(grpc::ChannelBase channel) : base(channel)
      {
      }
      /// <summary>Creates a new client for MessageResponse that uses a custom <c>CallInvoker</c>.</summary>
      /// <param name="callInvoker">The callInvoker to use to make remote calls.</param>
      public MessageResponseClient(grpc::CallInvoker callInvoker) : base(callInvoker)
      {
      }
      /// <summary>Protected parameterless constructor to allow creation of test doubles.</summary>
      protected MessageResponseClient() : base()
      {
      }
      /// <summary>Protected constructor to allow creation of configured clients.</summary>
      /// <param name="configuration">The client configuration.</param>
      protected MessageResponseClient(ClientBaseConfiguration configuration) : base(configuration)
      {
      }

      public virtual global::Generated.OperationResponse Greeting(global::Generated.MessageRequest request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return Greeting(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      public virtual global::Generated.OperationResponse Greeting(global::Generated.MessageRequest request, grpc::CallOptions options)
      {
        return CallInvoker.BlockingUnaryCall(__Method_Greeting, null, options, request);
      }
      public virtual grpc::AsyncUnaryCall<global::Generated.OperationResponse> GreetingAsync(global::Generated.MessageRequest request, grpc::Metadata headers = null, global::System.DateTime? deadline = null, global::System.Threading.CancellationToken cancellationToken = default(global::System.Threading.CancellationToken))
      {
        return GreetingAsync(request, new grpc::CallOptions(headers, deadline, cancellationToken));
      }
      public virtual grpc::AsyncUnaryCall<global::Generated.OperationResponse> GreetingAsync(global::Generated.MessageRequest request, grpc::CallOptions options)
      {
        return CallInvoker.AsyncUnaryCall(__Method_Greeting, null, options, request);
      }
      /// <summary>Creates a new instance of client from given <c>ClientBaseConfiguration</c>.</summary>
      protected override MessageResponseClient NewInstance(ClientBaseConfiguration configuration)
      {
        return new MessageResponseClient(configuration);
      }
    }

    /// <summary>Creates service definition that can be registered with a server</summary>
    /// <param name="serviceImpl">An object implementing the server-side handling logic.</param>
    public static grpc::ServerServiceDefinition BindService(MessageResponseBase serviceImpl)
    {
      return grpc::ServerServiceDefinition.CreateBuilder()
          .AddMethod(__Method_Greeting, serviceImpl.Greeting).Build();
    }

    /// <summary>Register service method with a service binder with or without implementation. Useful when customizing the  service binding logic.
    /// Note: this method is part of an experimental API that can change or be removed without any prior notice.</summary>
    /// <param name="serviceBinder">Service methods will be bound by calling <c>AddMethod</c> on this object.</param>
    /// <param name="serviceImpl">An object implementing the server-side handling logic.</param>
    public static void BindService(grpc::ServiceBinderBase serviceBinder, MessageResponseBase serviceImpl)
    {
      serviceBinder.AddMethod(__Method_Greeting, serviceImpl == null ? null : new grpc::UnaryServerMethod<global::Generated.MessageRequest, global::Generated.OperationResponse>(serviceImpl.Greeting));
    }

  }
}
#endregion
